import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyCAaE2Ua4FV_r4WS43POiIzsm_RDh7D9BQ",
    authDomain: "crown-clothes-be79c.firebaseapp.com",
    projectId: "crown-clothes-be79c",
    storageBucket: "crown-clothes-be79c.appspot.com",
    messagingSenderId: "101709331508",
    appId: "1:101709331508:web:f9629a78312564de029e61",
    measurementId: "G-FJZ8KSP8TE"
  };


export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};

  firebase.initializeApp(config)
  export const auth = firebase.auth()
  export const firestore = firebase.firestore()

  const provider = new firebase.auth.GoogleAuthProvider()
  provider.setCustomParameters({prompt: 'select_account'})
  export const signInWithGoogle = () => auth.signInWithPopup(provider)
  export default firebase