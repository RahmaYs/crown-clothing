import HomePage from './pages/homepage/homepage.component'
import Shop from './pages/shop/Shop.component'
import Header from './components/header/Header.component'
import CheckoutPage from './pages/checkout/Checkout.component'
import SignInAndSignUp from './pages/sign-in-and-sign-up/Sign-in-and-sign-up.component'
import { Route , Switch , Redirect } from 'react-router-dom'
import React from 'react'
import './App.css';
import { auth, createUserProfileDocument } from './firebase/firebase.utils'
import { connect } from 'react-redux'
import { setCurrentUser } from './redux/user/user.actions'
import { createStructuredSelector } from 'reselect'
import { selectCurrentUser } from './redux/user/user.selectors'


class App extends React.Component {

  unsubscribeFromAuth=null

componentDidMount(){
  const {setCurrentUser} = this.props
  this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
    if (userAuth) {
      const userRef = await createUserProfileDocument(userAuth);

      userRef.onSnapshot(snapShot => {
          setCurrentUser({
            id: snapShot.id,
            ...snapShot.data()
          });

        console.log(this.state);
      });
    }

    setCurrentUser(userAuth);
  });
}


componentWillUnmount(){
  this.unsubscribeFromAuth();
}

  render(){
    return (
      <div>
        <Header />
        <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/shop' component={Shop} />
        <Route 
         exact
         path='/signIn'
         render={
           () => 
              this.props.currentUser ? (
                <Redirect  to='/' />
              ) : (
                <SignInAndSignUp />
              )
         }
         />
        <Route exact path='/checkout' component={CheckoutPage} />

        </Switch>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector ({
  currentUser: selectCurrentUser
})

const mapDispatchToProps = dispatch =>({
  setCurrentUser : user => dispatch(setCurrentUser(user))
})
export default connect(mapStateToProps, mapDispatchToProps )(App);
