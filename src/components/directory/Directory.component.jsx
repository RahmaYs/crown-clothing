import React from 'react'
import MenuItem from '../menu-item/Menu-item.component'
import './directory.styles.scss'
import { connect } from 'react-redux'
import {createStructuredSelector} from 'reselect'
import selectDirectorySections from '../../redux/directory/directory.selectors'

 const Directory = ({sections}) =>{

        return (
            <div>
                <div className="directory-menu">
                    {  sections.map( (section) => (
                        <MenuItem key={section.id} section={section} />
                        //console.log('test!!!', section)
                    ))}
                </div>
            </div>
        )
    
}
const mapStateToProps = createStructuredSelector ({
  sections: selectDirectorySections
})
export default connect (mapStateToProps) (Directory)