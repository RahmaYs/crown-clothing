import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import './header.styles.scss'
import { ReactComponent as Logo} from '../../assets/crown.svg'
import { auth } from '../../firebase/firebase.utils'
import CartIcon from '../cart-icon/Cart-icon.component'
import CartDropdown from '../cart-dropdown/Cart-dropdown.component'
import { createStructuredSelector } from 'reselect'
import { selectCurrentUser } from '../../redux/user/user.selectors'
import { selectCartHidden } from '../../redux/cart/cart.selecors'



const Header = ({currentUser, hidden}) => {

    return(
        <div className="header">
            <Link  to="/" className="logo-container">
                   <Logo  className="logo" />
            </Link>
            <div className="options">
                <Link to="/shop" className="option"> Shop</Link>
                <Link to="" className="option"> Contact</Link>
                {
                currentUser ?
                (<div className='option' onClick={() => auth.signOut()}> Sign Out</div>)
                 :
                 (<Link to='/signIn' className='option'>Sign In</Link>)
            }
            <CartIcon />
            </div>
             {
                 hidden ? null :  <CartDropdown />
             }
           
        </div>
    ) 
}

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
    hidden: selectCartHidden
})
export default connect(mapStateToProps)(Header)