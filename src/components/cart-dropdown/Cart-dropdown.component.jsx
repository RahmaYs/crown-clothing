import React from 'react'
import './cart-component.styles.scss'
import CustomButton from  '../custom-button/Custom-button.component'
import CartItem from '../cart-item/Cart-item.component';
import { createStructuredSelector } from 'reselect'
import {selectCartItems } from '../../redux/cart/cart.selecors'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import  toggleCartHidden  from '../../redux/cart/cart.actions'

const CartDropdown =({cartItems, history , dispatch})=> {
    return (
      <div className="cart-dropdown">
        <div className="cart-items">
          { (cartItems.length) ? (
          cartItems.map((cartItem) => (
            <CartItem key={cartItem.id} item={cartItem} />
          )) ) : ( <span className="empty-message"> Your Cart is empty </span> )
          }
        </div>
        <CustomButton onClick={() => 
        {  history.push('/checkout') ; 
          dispatch(toggleCartHidden())}
          }> GO TO CHECKOUT </CustomButton>
      </div>
    );
}
const mapStateToProps = createStructuredSelector ({
    cartItems : selectCartItems
  });
export default withRouter(connect(mapStateToProps)(CartDropdown))