import React from 'react'
import FormInput from '../form-input/Form-input.component'
import CustomButton from '../custom-button/Custom-button.component'
import {auth, signInWithGoogle} from '../../firebase/firebase.utils'
import './signIn.styles.scss'
class SignIn extends React.Component {

    constructor (props){
        super(props)
        this.state={
            email:'',
            password:''
        }
    }

     handleSubmit = async (e) => {
      e.preventDefault();

      const { email, password } = this.state;
  
      try {
        await auth.signInWithEmailAndPassword(email, password);
        this.setState({ email: '', password: '' });
      } catch (error) {
        console.log(error);
      }
    }
   handleChange = (e) => {
     const {name, value } = e.target
       this.setState({[name]: value})
   }
 render(){
     const {email, password} = this.state
     return (
       <div className="sign-in">
         <h1> I Already have an account </h1>
         <span>Sign in with your email and password</span>
         <form onSubmit={this.handleSubmit}>
           <FormInput
             name="email"
             type="email"
             value={email}
             label="Email"
             handleChange={this.handleChange}
             required
           />

           <FormInput
             name="password"
             type="password"
             value={password}
             label="Password"
             handleChange={this.handleChange}
             required
           />

             <div className='buttons'> 
             <CustomButton type="submit">Sign In</CustomButton>
             <CustomButton onClick={signInWithGoogle}  isGoogleSignIn >Sign In with google</CustomButton>
             </div>
          
         </form>
       </div>
     );
 }

}
export default SignIn ;