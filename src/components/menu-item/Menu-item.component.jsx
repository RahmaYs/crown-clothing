import React from 'react'
import './menu-item.styles.scss'
import {withRouter} from 'react-router-dom'

const MenuItem = (props) => {
  const {title , imageUrl , size, linkUrl} = props.section
  console.log(props)
    return (
      <div className={`${size} menu-item`} 
           onClick={()=> props.history.push(`${props.match.url}${linkUrl}`)}
      >
        <div
          className="background-image"
          style={{
            backgroundImage: `url(${imageUrl})`,
          }}
        />
        <div className="content">
          <h1 className="title"> {title.toUpperCase()} </h1>
          <span className="subtitle">SHOP NOW</span>
        </div>
      </div>
    );
}

export default withRouter(MenuItem)
