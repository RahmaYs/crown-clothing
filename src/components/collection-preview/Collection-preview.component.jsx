import React from 'react'
import CollectionItem from '../collection-item/Collection-item.component'
import './collection-preview.styles.scss'

const CollectionPreview = (props) => {

    const { title , items } = props.col
   
    return (
      <div className="collection-preview">
        <h1 className="title">{title.toUpperCase()}</h1>
        <div className="preview">
          {items
            .filter((item, indx) => indx < 4)
            .map((item) => (
              <CollectionItem key={item.id}  item={item}  /> 
            ))}
        </div>
      </div>
    );
}

export default CollectionPreview
