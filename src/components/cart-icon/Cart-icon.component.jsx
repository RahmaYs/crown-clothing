import React from 'react'
import './cart-icon.styles.scss'
import { ReactComponent as ShoppingBag } from '../../assets/shoppingBag.svg'
import { connect } from 'react-redux'
import toggleCartHidden from '../../redux/cart/cart.actions'
import { createStructuredSelector } from 'reselect'
import {selectCartItemsCount} from '../../redux/cart/cart.selecors'

const CartIcon = ({toggleCartHidden , itemCount}) => {

    return (
        <div className="cart-icon" onClick={toggleCartHidden}>
          <ShoppingBag  className="shopping-icon" />
          <span className="item-count"> {itemCount} </span>
        </div>
    )

}

const mapStateToProps = createStructuredSelector ({
  itemCount : selectCartItemsCount
})

const mapDispatchToProps = dispatch => ({
  toggleCartHidden : () => dispatch(toggleCartHidden())
})

export default connect(mapStateToProps, mapDispatchToProps)(CartIcon)