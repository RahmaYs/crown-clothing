import React from 'react'
import './collection-overview.styless.scss'
import { createStructuredSelector } from 'reselect'
import  CollectionPreview  from '../collection-preview/Collection-preview.component'
import { connect } from 'react-redux'
import { selectCollectionsForPreview } from '../../redux/shop/shop.selectors'

const CollectionOverview = ({collection}) => {
  return (
    <div className="collection-overview">
    { collection.map( (col) => <CollectionPreview key= {col.id} col={col} /> ) }
   </div>
  )
}

const mapStateToProps = createStructuredSelector ({
    collection : selectCollectionsForPreview
})


export default connect(mapStateToProps)(CollectionOverview) ;