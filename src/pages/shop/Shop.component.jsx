import React from 'react'
import { Route } from 'react-router-dom'
import CollectionOverview from '../../components/collection-overview/Collection-overview.component'
import Collection from '../collection/Collection.component'
const Shop =({ match }) => {
console.log('ressssssss', match.path)
        return (
            <div className='shop-page'> 
               <Route exact path={`${match.path}`} component={CollectionOverview} />
               <Route  path={`${match.path}/:collectionId`} component={Collection} />
            </div>
        )
    }
   
export default Shop
