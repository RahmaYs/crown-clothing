import cartActionTypes from './cart.types'

 const toggleCartHidden = () => ({

    type: cartActionTypes.TOGGEL_CART_HIDDEN
})
export default toggleCartHidden

export const addItem = item => ({
    type: cartActionTypes.ADD_ITEM,
    payload: item
})

export const removeItem = item => ({
    type: cartActionTypes.REMOVE_ITEM,
    payload: item
  });

export const clearItemFromCart = item => ({
    type: cartActionTypes.CLEAR_ITEM_FROM_CART,
    payload: item
});

