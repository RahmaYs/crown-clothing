import { createSelector } from 'reselect'
const selectShop = state => state.shop


export const selectShopCollections = createSelector(
    [selectShop],
    shop =>shop.collection
)


export const selectCollectionsForPreview = createSelector (
  [selectShopCollections],
  collections => Object.keys(collections).map(key => collections[key])
)
//Function returns other function
export const selectCollection = (collectionUrlParam) =>
  createSelector(
    [selectShopCollections], 
    (collections) => collections[ collectionUrlParam]
           
  );
